package funny.location.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

public class ListLocationActivity extends AppCompatActivity implements LocationAdapter.LocationListener {

    public final static String DATA = "DATA";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_list);

        Toolbar toolbar = findViewById(R.id.activity_my_list_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.activity_my_list_rv);

        LocationAdapter adapter = new LocationAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.setData(MyDatabaseHelper.newInstance(this).getAllLocations());
    }

    @Override
    public void onItemClick(int position, LocationObject location) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DATA, location);
        intent.putExtra(DATA, bundle);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
