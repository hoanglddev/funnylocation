package funny.location.app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

public class DetailActivity extends AppCompatActivity {

    public final static String LOCATION_DATA = "location_data";
    private static final int REQUEST_GALLERY = 3;

    private ImageView imPhoto;
    private EditText edTitle;
    private EditText edDescription;
    private LocationObject currentLocation;
    private boolean isUpdated;
    private Uri currentUriPhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        imPhoto = findViewById(R.id.activity_detail_im_photo);
        edTitle = findViewById(R.id.activity_detail_ed_title);
        edDescription = findViewById(R.id.activity_detail_ed_description);
        ImageView btnSave = findViewById(R.id.activity_detail_btn_save);
        TextView btnBack = findViewById(R.id.activity_detail_btn_back);
        ImageView btnDelete = findViewById(R.id.activity_detail_btn_delete);

        imPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_GALLERY);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUpdated || currentUriPhoto != null)
                    setResult(RESULT_OK);

                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentLocation.title = edTitle.getText().toString();
                currentLocation.description = edDescription.getText().toString();

                if (currentUriPhoto != null)
                    currentLocation.urlPhoto = getFilePathFromURI(currentUriPhoto);

                MyDatabaseHelper.newInstance(getApplicationContext()).updateLocation(currentLocation);
                Toast.makeText(DetailActivity.this, "Update Success", Toast.LENGTH_SHORT).show();

                setResult(RESULT_OK);
                finish();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DetailActivity.this, "Delete success", Toast.LENGTH_SHORT).show();
                MyDatabaseHelper.newInstance(getApplicationContext()).deleteLocation(currentLocation);
                setResult(RESULT_OK);
                finish();
            }
        });

        currentLocation = (LocationObject) getIntent().getSerializableExtra(LOCATION_DATA);
        edTitle.setText(currentLocation.title);
        edDescription.setText(currentLocation.description);

        if (currentLocation.urlPhoto != null)
            Picasso.get().load(Uri.fromFile(new File(currentLocation.urlPhoto)))
                    .error(R.drawable.ic_launcher_background)
                    .into(imPhoto);

        edTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (currentLocation != null && !charSequence.toString().equals(currentLocation.title))
                    isUpdated = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (currentLocation != null && !charSequence.toString().equals(currentLocation.description))
                    isUpdated = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isUpdated || currentUriPhoto != null)
            setResult(RESULT_OK);

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            currentUriPhoto = data.getData();
            if (imPhoto != null)
                Picasso.get().load(currentUriPhoto).error(R.drawable.ic_launcher_background).into(imPhoto);
        }
    }

    public String getFilePathFromURI(Uri uri) {
        String result = "";
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        return result;
    }
}
