package funny.location.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "SQLite";

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "Location_Manager";

    private static final String TABLE_LOCATION = "Location";

    private static final String COLUMN_ID = "Id";
    private static final String COLUMN_TITLE = "Title";
    private static final String COLUMN_DESCRIPTION = "Description";
    private static final String COLUMN_URL_PHOTO = "Url_Photo";
    private static final String COLUMN_LOCATION_LAT = "Location_lat";
    private static final String COLUMN_LOCATION_LNG = "Location_lng";

    private static MyDatabaseHelper db;

    private MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static MyDatabaseHelper newInstance(Context context) {
        if (db == null)
            db = new MyDatabaseHelper(context);
        return db;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String script = "CREATE TABLE " + TABLE_LOCATION + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_TITLE + " TEXT,"
                + COLUMN_DESCRIPTION + " TEXT, "
                + COLUMN_URL_PHOTO + " TEXT, "
                + COLUMN_LOCATION_LAT + " TEXT, "
                + COLUMN_LOCATION_LNG + " TEXT "
                + ")";
        db.execSQL(script);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        onCreate(db);
    }


    public void addLocation(LocationObject location) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, location.title);
        values.put(COLUMN_DESCRIPTION, location.description);
        values.put(COLUMN_URL_PHOTO, location.urlPhoto);
        values.put(COLUMN_LOCATION_LAT, String.valueOf(location.lat));
        values.put(COLUMN_LOCATION_LNG, String.valueOf(location.lng));

        db.insert(TABLE_LOCATION, null, values);

        db.close();
    }


    public LocationObject getLocationById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LOCATION, new String[]{COLUMN_ID,
                        COLUMN_TITLE, COLUMN_DESCRIPTION, COLUMN_URL_PHOTO, COLUMN_LOCATION_LAT, COLUMN_LOCATION_LNG}, COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        LocationObject locationObject = new LocationObject();

        if (cursor != null)
            cursor.moveToFirst();
        else
            return locationObject;

        locationObject.id = Integer.parseInt(cursor.getString(0));
        locationObject.title = cursor.getString(1);
        locationObject.description = cursor.getString(2);
        locationObject.urlPhoto = cursor.getString(3);
        locationObject.lat = Double.parseDouble(cursor.getString(4));
        locationObject.lng = Double.parseDouble(cursor.getString(5));

        return locationObject;
    }

    public LocationObject getLastLocation() {

        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToLast();
        else
            return new LocationObject();

        LocationObject locationObject = new LocationObject();

        locationObject.id = Integer.parseInt(cursor.getString(0));
        locationObject.title = cursor.getString(1);
        locationObject.description = cursor.getString(2);
        locationObject.urlPhoto = cursor.getString(3);
        locationObject.lat = Double.parseDouble(cursor.getString(4));
        locationObject.lng = Double.parseDouble(cursor.getString(5));

        return locationObject;
    }


    public List<LocationObject> getAllLocations() {

        List<LocationObject> locations = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                LocationObject locationObject = new LocationObject();

                locationObject.id = Integer.parseInt(cursor.getString(0));
                locationObject.title = cursor.getString(1);
                locationObject.description = cursor.getString(2);
                locationObject.urlPhoto = cursor.getString(3);
                locationObject.lat = Double.parseDouble(cursor.getString(4));
                locationObject.lng = Double.parseDouble(cursor.getString(5));

                locations.add(locationObject);
            } while (cursor.moveToNext());
        }

        return locations;
    }

    public int getLocationsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_LOCATION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();

        cursor.close();

        return count;
    }


    public int updateLocation(LocationObject locationObject) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, locationObject.id);
        values.put(COLUMN_TITLE, locationObject.title);
        values.put(COLUMN_DESCRIPTION, locationObject.description);
        values.put(COLUMN_URL_PHOTO, locationObject.urlPhoto);
        values.put(COLUMN_LOCATION_LAT, locationObject.lat);
        values.put(COLUMN_LOCATION_LNG, locationObject.lng);

        // updating row
        return db.update(TABLE_LOCATION, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(locationObject.id)});
    }

    public void deleteLocation(LocationObject locationObject) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATION, COLUMN_ID + " = ?",
                new String[]{String.valueOf(locationObject.id)});
        db.close();
    }
}