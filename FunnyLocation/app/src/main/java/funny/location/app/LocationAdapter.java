package funny.location.app;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface LocationListener {
        void onItemClick(int position, LocationObject location);
    }

    private Context context;
    private List<LocationObject> locations;
    private LocationListener locationListener;

    public LocationAdapter(LocationListener locationListener) {
        this.locations = new ArrayList<>();
        this.locationListener = locationListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view  = inflater.inflate(R.layout.location_item, parent, false);
        return new LocationAdapter.LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((LocationAdapter.LocationViewHolder) holder).bindData(position, locations.get(position));
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    public void setData(List<LocationObject> locations) {
        if (locations == null)
            locations = new ArrayList<>();

        this.locations = locations;
        notifyDataSetChanged();
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvAddress;
        private CircleImageView imAvatar;

        private int position;
        private LocationObject location;

        public LocationViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.location_result_item_tv_name);
            tvAddress = itemView.findViewById(R.id.location_result_item_tv_address);
            imAvatar = itemView.findViewById(R.id.location_item_im);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    locationListener.onItemClick(position, location);
                }
            });
        }

        public void bindData(int position, LocationObject location) {
            this.position = position;
            this.location = location;

            if (location == null) return;

            tvName.setText(location.title);
            tvAddress.setText(location.description);

            if (location.urlPhoto != null)
                Picasso.get().load(Uri.fromFile(new File(location.urlPhoto)))
                        .error(R.drawable.ic_launcher_background)
                        .into(imAvatar);
        }
    }
}