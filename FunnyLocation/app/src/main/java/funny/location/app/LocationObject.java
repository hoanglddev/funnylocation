package funny.location.app;

import java.io.Serializable;

public class LocationObject implements Serializable {

    public int id;
    public String title;
    public String description;
    public String urlPhoto;
    public double lat;
    public double lng;

}
