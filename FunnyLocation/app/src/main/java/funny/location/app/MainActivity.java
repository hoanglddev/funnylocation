package funny.location.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public final static int PERMISSION_LOCATION_REQUEST_CODE = 1;
    public final static int REQUEST_CODE_UPDATE = 2;
    private static final int REQUEST_GALLERY = 3;
    public final static int REQUEST_LIST = 4;

    private MapView mMapView;
    private GoogleMap googleMap;
    private AlertDialog dialogCreateEdit;
    private List<LocationObject> dataLocations;
    private MyDatabaseHelper databaseHelper;
    private boolean isDetailShowing;
    private ImageView imPhotoDialog;
    private Uri currentUriPhoto;
    private ImageView imTempLoaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_LOCATION_REQUEST_CODE);
            return;
        }

        imTempLoaded = findViewById(R.id.image_temp_loaded);


        mMapView = findViewById(R.id.activity_main_map_view);
        mMapView.onCreate(savedInstanceState);

        Button btnMyList = findViewById(R.id.main_activity_btn_list);
        btnMyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListLocationActivity.class);
                startActivityForResult(intent, REQUEST_LIST);
            }
        });

        databaseHelper = MyDatabaseHelper.newInstance(this);
        dataLocations = databaseHelper.getAllLocations();

        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                initGoogleMap(mMap);
                initGPSButton();
                loadMyLocation(dataLocations);
            }

        });
    }

    public void initGoogleMap(GoogleMap mMap) {
        googleMap = mMap;

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng point) {
                if (!isDetailShowing)
                    showDialogCreateEdit(point.latitude, point.longitude);
                else
                    isDetailShowing = false;
//                drawPath(point);
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void initGPSButton() {

        googleMap.setMyLocationEnabled(true);

        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Toast.makeText(getApplicationContext(), "GPS is disabled!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP,  RelativeLayout.TRUE);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        rlp.setMargins(0, 40, 40, 0);
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void loadMyLocation(List<LocationObject> locations) {
        if (locations == null || locations.isEmpty()) {
            LatLng focusPoint = new LatLng(10.7635604, 106.6703963);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(focusPoint).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            return;
        }

        for (int i = 0; i < locations.size(); i++) {
            createMarkerAndMoveCamera(locations.get(i), i == locations.size() - 1);
        }
    }

    private void createMarkerAndMoveCamera(LocationObject locationObject, boolean isMoveCamera) {
        LatLng venueLocation = new LatLng(locationObject.lat, locationObject.lng);

        MarkerOptions markerOptions = new MarkerOptions().position(venueLocation);
//        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
        googleMap.addMarker(markerOptions).setTag(locationObject);

        // Load temp image to cache Picasso image in marker info
        if (locationObject.urlPhoto != null)
            Picasso.get().load(Uri.fromFile(new File(locationObject.urlPhoto))).into(imTempLoaded);

        setInfoVenueMapMarker();
        if (isMoveCamera) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(venueLocation).zoom(17).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null)
            mMapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null)
            mMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null)
            mMapView.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_LOCATION_REQUEST_CODE);
            Toast.makeText(this, "you must accept permission to run app", Toast.LENGTH_SHORT).show();
        } else {
            startActivity(new Intent(MainActivity.this, MainActivity.class));
            finish();
        }

    }

    private void showDialogCreateEdit(final double lat, final double lng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_create_edit, null);
        final EditText edTitle = view.findViewById(R.id.dialog_ed_title);
        final EditText edDescription = view.findViewById(R.id.dialog_ed_description);

        imPhotoDialog = view.findViewById(R.id.dialog_im_photo);
        imPhotoDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentUriPhoto = null;
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_GALLERY);
                Log.d("hell", "");
            }
        });

        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                LocationObject locationObject = new LocationObject();
                locationObject.title = edTitle.getText().toString();
                locationObject.description = edDescription.getText().toString();
                locationObject.lat = lat;
                locationObject.lng = lng;

                addNewLocationObjectAndReload(locationObject);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogCreateEdit.dismiss();
            }
        });

        dialogCreateEdit = builder.create();
        dialogCreateEdit.show();
    }

    private void addNewLocationObjectAndReload(LocationObject locationObject) {
        if (currentUriPhoto != null)
            locationObject.urlPhoto = getFilePathFromURI(currentUriPhoto);

        // save to database
        databaseHelper.addLocation(locationObject);

        clearAndUpdateData();
    }

    private void setInfoVenueMapMarker() {
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                isDetailShowing = true;
                final LocationObject locationObject = (LocationObject) marker.getTag();

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                        intent.putExtra(DetailActivity.LOCATION_DATA, locationObject);
                        startActivityForResult(intent, REQUEST_CODE_UPDATE);
                    }
                });

                return getViewContentVenueInfo(locationObject);
            }
        });
    }

    private View getViewContentVenueInfo(LocationObject locationObject) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.map_marker_info, null);

        TextView tvTitle = view.findViewById(R.id.map_marker_info_tv_title);
        ImageView imPhoto = view.findViewById(R.id.map_marker_info_im_photo);

        tvTitle.setText(locationObject.title);

        imPhoto.getLayoutParams().height = displayMetrics.heightPixels / 5;
        imPhoto.getLayoutParams().width = displayMetrics.widthPixels * 3 / 5;
        imPhoto.requestLayout();

        if (locationObject.urlPhoto == null)
            imPhoto.setBackgroundResource(R.drawable.ic_launcher_background);
        else {
            imPhoto.setBackgroundColor(Color.argb(0, 0, 0, 0));
            Picasso.get().load(Uri.fromFile(new File(locationObject.urlPhoto)))
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(imPhoto);
        }
        return view;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_LIST) {
                Bundle bundle = data.getBundleExtra(ListLocationActivity.DATA);
                LocationObject locationObject = (LocationObject) bundle.get(ListLocationActivity.DATA);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(locationObject.lat, locationObject.lng)).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                return;
            }

            if (requestCode == REQUEST_CODE_UPDATE) {
                clearAndUpdateData();
            }

            if (requestCode == REQUEST_GALLERY) {
                currentUriPhoto = data.getData();
                if (imPhotoDialog != null)
                    Picasso.get().load(currentUriPhoto).error(R.drawable.ic_launcher_background).into(imPhotoDialog);
            }
        }

    }

    private void clearAndUpdateData() {
        if (googleMap != null)
            googleMap.clear();

        dataLocations = databaseHelper.getAllLocations();
        loadMyLocation(dataLocations);
    }

    public String getFilePathFromURI(Uri uri) {
        String result = "";
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        return result;
    }
}
